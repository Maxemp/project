<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 17.01.2018
 * Time: 16:10
 */

namespace shop\services\auth;

use shop\entities\User;
use shop\repositories\UserRepository;
use shop\forms\auth\SignupForm;
use yii\mail\MailerInterface;

class SignupService
{
	private $users;
	private $mailer;
	public function __construct(MailerInterface $mailer, UserRepository $users)
	{
		$this->mailer = $mailer;
		$this->users = $users;
	}

	public function signup(SignupForm $form): void
	{
		$user = User::requestSignup(
			$form->username,
			$form->password,
			$form->email
		);

		$this->users->save($user);
		$sent = $this->mailer
			->compose(
				['html' => 'emailConfirmToken-html', 'text' => 'emailConfirmToken-text'],
				['user' => $user]
			)
			->setTo($form->email)
			->setSubject('Signup confirm for'. \Yii::$app->name)
			->send();
		if (!$sent){
			throw new \DomainException('Email sending error');
		}
	}

	public function confirm($token)
	{
		if (empty($token)){
			throw new \DomainException('Empty confirm token.');
		}
		$user = $this->users->getByEmailConfirmToken($token);
		$user->confirmSignup();
		$this->users->save($user);

	}
}