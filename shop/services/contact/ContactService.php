<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.01.2018
 * Time: 20:51
 */

namespace shop\services\contact;

use shop\forms\ContactForm;
use yii\mail\MailerInterface;

class ContactService
{
	private $adminEmail;
	private $mailer;

	public function __construct($adminEmail, MailerInterface $mailer)
	{
		$this->adminEmail = $adminEmail;
		$this->mailer = $mailer;
	}

	public function send (ContactForm $form):void
	{
		$sent = $this->mailer->compose()
			->setTo($this->adminEmail)
			->setFrom($form->subject)
			->setTextBody($form->body)
			->send();
		if (!$sent){
			throw new \RuntimeException('SendingError.');
		}
	}
}