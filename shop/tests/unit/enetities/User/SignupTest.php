<?php
namespace common\test\unit\entities\User;

use Codeception\Test\Unit;
use shop\entities\User;

class SignupTest extends Unit
{
	public function testSuccess()
	{
		$user = User::requestSignup(
			$username = 'username123',
			$email = 'emai123l@mail.ru',
			$password = 'pass1234word'
		);
		$this->assertEquals($username, $user->username);
		$this->assertEquals($email, $user->email);
		$this->assertNotEmpty($user->password_hash);
		$this->assertNotEmpty($password,$user->password_hash);
		$this->assertNotEmpty($user->created_at);
		$this->assertNotEmpty($user->auth_key);
		$this->assertNotEmpty($user->auth_key);
		$this->assertTrue($user->isActive());
	}
}