<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 22.01.2018
 * Time: 19:16
 */

namespace shop\repositories;

use shop\entities\User;

class UserRepository
{
	public function getByEmailConfirmToken($token):User
	{
		return $this->getBy(['email_confirm_token' => $token]);
	}

	public function findByUsernameOrEmail($value): ?User
	{
		return User::find()->andWhere(['or', ['username' => $value], ['email' => $value]])->one();
	}

	public function getByEmail(string $email):User
	{
		return $this->getBy(['email' => $email]);
	}

	public function existsByPasswordResetToken(string $token)
	{
		return (bool) User::findByPasswordResetToken($token);
	}

	public function getByPasswordResetToken(string $token):User
	{
		return $this->getBy(['password_reset_token' => $token]);
	}

	public function save(User $user):void
	{
		if (!$user->save()){
			throw new \RuntimeException('Saving Error');
		}
	}

	private function getBy(array $condition):User
	{
		if (!$user = User::find()->andWhere($condition)->limit(1)->one()){
			throw new NotFoundException('User not found.');
		}
		return $user;
	}
}