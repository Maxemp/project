<?php
namespace frontend\controllers;

use shop\services\auth\AuthService;
use shop\services\auth\PasswordResetService;
use shop\services\auth\SignupService;
use shop\services\contact\ContactService;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use shop\forms\auth\LoginForm;
use shop\forms\auth\PasswordResetRequestForm;
use shop\forms\auth\ResetPasswordForm;
use shop\forms\auth\SignupForm;
use shop\forms\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
	private $passwordResetService;
	private $contactService;
	private $signupService;
	private $authService;

	public function __construct(
		$id
		,$module
		,PasswordResetService $passwordResetService
		,ContactService $contactService
		, SignupService $signupService
		,$config = []
		, AuthService $authService
	) {
		parent::__construct( $id, $module, $config );
		$this->authService = $authService;
		$this->passwordResetService = $passwordResetService;
		$this->contactService = $contactService;
		$this->signupService = $signupService;
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$user = $this->authService->auth($form);
        		Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
        		return $this->goBack();
	        }catch (\DomainException $e){
		        Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }
        return $this->render('login', [
            'model' => $form,
        ]);

    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $form = new ContactForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$this->contactService->send($form);
		        Yii::$app->session->setFlash('success', 'Thank you for contacting');
		        return $this->goHome();
	        }catch(\RuntimeException $e){
		        Yii::$app->errorHandler->logException($e);
		        Yii::$app->session->setFlash('error', $e->getMessage());
	        }
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $form,
        ]);

    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $form = new SignupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->signupService->signup($form);
	        try{
				$this->signupService->signup($form);
		        Yii::$app->session->setFlash('success', 'Check your email for further instructions');
		        return $this->goHome();
	        }catch (\DomainException $e){
		        Yii::$app->errorHandler->logException($e);
		        Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }
        return $this->render('signup', [
            'model' => $form,
        ]);
    }

    public function actionConfirm($token)
    {
    	try{
		    $this->signupService->confirm($token);
		    Yii::$app->session->setFlash('success', 'Yiur email is confirmed.');
		    return $this->redirect(['login']);
	    }catch (\DomainException $e){
		    Yii::$app->errorHandler->logException($e);
		    Yii::$app->session->setFlash('error', $e->getMessage());
		    return $this->goHome();
	    }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $form = new PasswordResetRequestForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
				$this->passwordResetService->request($form);
				Yii::$app->session->setFlash('success', 'Check your email for further instructions');
				return $this->goHome();
            }catch (\DomainException $e){
	            Yii::$app->errorHandler->logException($e);
	            Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $form,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $this->passwordResetService->validateToken($token);
        } catch (\DomainException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

		$form = new ResetPasswordForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try {
		        $this->passwordResetService->reset($token, $form);
		        Yii::$app->session->setFlash('success', 'New password saved.');
		        return $this->goHome();
	        }catch (\DomainException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }

        return $this->render('resetPassword', [
            'model' => $form,
        ]);
    }
}
