<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.01.2018
 * Time: 16:38
 */

namespace common\tests\unit\enetities\User;

use shop\entities\User;
use Codeception\Test\Unit;

class RequestSignupTest extends Unit
{
	public function testSuccess()
	{
		$user = User::requestSignup(
			$username = 'user42name',
			$email = 'em32ail@mail.ru',
			$password = 'pas213sword123'
		);

		$this->assertEquals($username, $user->username);
		$this->assertEquals($email, $user->email);
		$this->assertNotEmpty($user->password_hash);
		$this->assertNotEquals($password, $user->password_hash);
		$this->assertNotEmpty($user->created_at);
		$this->assertNotEmpty($user->auth_key);
		$this->assertNotEmpty($user->email_confirm_token);
		$this->assertTrue($user->isWait());
		$this->assertFalse($user->isActive());
	}
}